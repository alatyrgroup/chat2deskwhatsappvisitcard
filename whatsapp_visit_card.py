#! /usr/bin/env python
# -*- coding: utf-8 -*-
import re, sys, time, json
from datetime import datetime
from requests import get, put, post, delete

class Handler:
 reload(sys)
 sys.setdefaultencoding('UTF8')

 def manually_handler(self, input_data, c2d):

  internal =""
  phone = ""
  event =""
  text =""

  if 'event' in input_data:
   event = input_data['event']

  chat_state = False # Закрывать или нет чат после отправки сообщения. True — закрывать

  if event == "NOTIFY_START":
   text='👋 Спасибо за звонок! Напишите в чат, мы постараемся оперативно вам ответить'
   kind='autoreply'
  else: return ""

  if 'caller_id' in input_data:
   phone = str(input_data['caller_id'])
  elif 'phone' in input_data:
   phone = str(input_data['phone'])


  if phone[0] == '+': phone=phone[1:]
  if phone == '' or phone[:2] != "79": return '' #Номера нет или не мобильный номер

  api_headers['Authorization']=c2d.token
  data=get(api_url % 'clients?phone='+phone,headers=api_headers)
  client=json.loads(data.text)
  if client['meta']['total']==0:
   if not counter_control(): return 'Too many new clients for today (not sent)'
   text='👋 Привет! Вы только что звонили нам. Мы будем рады как вашим звонкам, так и сообщениям в этом чате. Задавайте вопросы по сервису здесь, мы всегда онлайн!'
   post(api_url % 'clients?transport=whatsapp&phone='+phone,headers=api_headers)
   data=get(api_url % 'clients?phone='+phone,headers=api_headers)
   client=json.loads(data.text)

  if not client or not client['data'] or len(client['data'])==0: return ''

  sendText(c2d, client['data'][0]['id'],text,kind,chat_state)

  return ''

def counter_control():
 fldHere='extra_comment_2'   # в этом поле храним счетчик
 limitHere=50                 # лимит для счетчика - кол-во новых клиентов в день, которым шлем визитку. При превышении - не шлем
 data=get(api_url % 'clients?limit=1',headers=api_headers)
 id=json.loads(data.text)['data'][0]['id']                   #  id  клиента-хранителя счетчика
 content=json.loads(data.text)['data'][0][fldHere]           # что в поле?
 cnt=1
 curDay=int(time.strftime("%d"))                             # день текущей даты
 if content:
  value=json.loads(content)                               # данные из его поля как объект
  if curDay==int(value['date']):
   if int(value["count"])>=limitHere: return False     # на сегодня счетчик уже полон
   else: cnt=int(value["count"])+1
 data=json.dumps({"date":curDay,"count":cnt})
 data={"extra_comment_2": data}
 put(api_url % 'clients/%s' % str(id),headers=api_headers,data=data)
 return True

# послать сообщение и закрыть диалог.
def sendText(c2d,clientID, text, kind='autoreply', close_chat=True):
 info =  c2d.send_message(str(clientID), text, kind)
 #close_chat - если true, то закрыть диалог после отправки сообщения
 if close_chat and "message_id" in info:
  headers={'Authorization':c2d.token}
  mn=str( info["message_id"] )
  obj=get(api_url %'messages/'+mn,headers=headers).json()
  #   if not obj or not obj["data"] or not obj["data"]["dialog_id"]: return ''
  try:
   dg = str( obj["data"]["dialog_id"] )
  except:
   return ''
  oID = obj["data"]["operator_id"]

############### общие функции
api_headers={'Authorization':''}
api_url='https://api.chat2desk.com/v1/%s'